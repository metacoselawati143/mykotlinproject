package com.example.mykotlinproject

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_kedua.*

class ActivityKedua : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kedua)

        val bundle = intent.extras

        val nil = bundle?.getCharSequence(MainActivity.NILAI)
        val nama = bundle?.getCharSequence(MainActivity.NAMA)
        val nim = bundle?.getCharSequence(MainActivity.NIM)

        nama_hasil.text = ("Nama : " +nama)
        nim_hasil.text = ("NIM : " +nim)
        nilai_hasil.text = ("Nilai angka : " +nil)

        val nilai = nil.toString().toInt()

        when {
            nilai >= 80 -> keterangan.text = ("Keterangan : A")
            nilai >= 60 -> keterangan.text = ("Keterangan : B")
            nilai >= 40 -> keterangan.text = ("Keterangan : C")
            nilai >= 20 -> keterangan.text = ("Keterangan : D")
            else -> keterangan.text = ("Keterangan : E")
        }

    }
}